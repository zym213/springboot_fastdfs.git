package com.zym.fdfs.util;

public enum FileGroup {
    AVATAR("testgroup");
    private String group;

    FileGroup(String group) {
        this.group = group;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }
}
