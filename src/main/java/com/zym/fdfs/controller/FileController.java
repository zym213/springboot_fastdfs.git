package com.zym.fdfs.controller;

import com.github.tobato.fastdfs.domain.fdfs.StorePath;
import com.zym.fdfs.util.FastFileUtil;
import com.zym.fdfs.util.FileGroup;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Api(tags = "FastDFS测试")
@RestController
@RequestMapping("/api/fastdfs")
public class FileController {

    @Autowired
    private FastFileUtil fastFileUtil;

    @ResponseBody
    @ApiOperation(value = "上传文件",httpMethod = "POST")
    @PostMapping("/upload")
    public String uploadFile(@ApiParam("文件") MultipartFile file) throws IOException {
        StorePath storePath = fastFileUtil.uploadFastFile(file,FileGroup.AVATAR);
        System.out.println("文件上传成功，存储地址为："+storePath.getFullPath());
        return "文件上传成功，存储地址为："+storePath.getFullPath();
    }

    @ResponseBody
    @ApiOperation(value = "删除文件",httpMethod = "POST")
    @PostMapping("/delete")
    public String deleteFile(String filePath){
        try{
            fastFileUtil.deleteFile(filePath);
            System.out.println("删除成功");
            return "删除成功";
        }catch (Exception e){
            System.out.println("删除失败");
            return "删除失败";
        }
    }

    @ResponseBody
    @ApiOperation(value = "下载文件",httpMethod = "POST")
    @PostMapping("/download")
    public void downloadFile(HttpServletRequest request, HttpServletResponse response,String filePath){
        fastFileUtil.downloadFile(request,response,filePath);
    }
}
