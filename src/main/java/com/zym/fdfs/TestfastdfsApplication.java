package com.zym.fdfs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestfastdfsApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestfastdfsApplication.class, args);
    }

}
